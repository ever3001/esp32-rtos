# ESP 32 FreeRTOS Examples

I create this repository to practice the OS FreeRTOS. I use an ESP32 with the Espressif IoT Development Framework(ESP-IDF) and PlatformIO.


To practice I use the follow videos and post:

- Tasks: https://youtu.be/Z7Z4_ENFjDM
- Queue: https://youtu.be/2hyoRffRukw
- ISR and Semaphore: https://youtu.be/XFTvT82xsAo
- Mutex: https://youtu.be/Zy8mKkAtEJw
- ADC1 & UART1: https://youtu.be/zbLAyw9NSoM


## Compilation

The best way to compile and upload is by using PlatformIO.

### Task Example
``` bash
# Build
platformio run --environment esp32_freertos_01
# Upload
platformio run --target upload --environment esp32_freertos_01
```

### Queue Example
``` bash
# Build
platformio run --environment esp32_freertos_02
# Upload
platformio run --target upload --environment esp32_freertos_02
```

### ISR and Semaphore Example
``` bash
# Build
platformio run --environment esp32_freertos_03
# Upload
platformio run --target upload --environment esp32_freertos_03
```

### Mutex Example
``` bash
# Build
platformio run --environment esp32_freertos_04
# Upload
platformio run --target upload --environment esp32_freertos_04
```

### ADC1 & UART1 Example
``` bash
# Build
platformio run --environment esp32_freertos_05
# Upload
platformio run --target upload --environment esp32_freertos_05
```