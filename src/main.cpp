#include <stdio.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_system.h"
#include "nvs_flash.h"

#if defined(FREERTOS_01)
#include "freertosTask.h"
#elif defined(FREERTOS_02)
#include "freertosQueue.h"
#elif defined(FREERTOS_03)
#include "freertosSemaphoreIsr.h"
#elif defined(FREERTOS_04)
#include "freertosMutex.h"
#elif defined(FREERTOS_05)
#include "freertosADCUART.h"
#elif defined(FREERTOS_CAP_SOIL_MOIS_SENSOR)
#include "freertos_mois_sensor.h"
#endif

#ifdef __cplusplus
extern "C"
{
#endif

    void app_main(void)
    {
        start_app();
    }

#ifdef __cplusplus
}
#endif