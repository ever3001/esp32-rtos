#ifndef _05_FREERTOS_ADC_UART_H_
#define _05_FREERTOS_ADC_UART_H_
/** Standard libraries*/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

/** Freertos Headers */
#include "esp_system.h"
#include "nvs_flash.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"

/** GPIO and drivers */
#include "driver/gpio.h"
#include "driver/uart.h"
#include "driver/adc.h"
#include "soc/uart_struct.h"

#define DEBUG

#define ADC_CHANNEL (4)

#define TXD (4)
#define RXD (5)
#define RTS (18)
#define CTS (19)

#define BUFF_SZ (1024)

#define QUEUE_READ_MSG_SZ (1)
#define READ_MSG_SZ (2)

#define QUEUE_VOLTAGE_MSG_SZ (1)
#define VOLTAGE_MSG_SZ (32)

xQueueHandle queueRead;
xQueueHandle queueVoltage;

void adc1Task(void *args)
{
    adc1_config_width(ADC_WIDTH_BIT_12);
    adc1_config_channel_atten(ADC1_CHANNEL_0, ADC_ATTEN_DB_11);
    int value;
    while (1)
    {
#ifdef DEBUG
        value = esp_random() % 4095;
        value < 0 ? value = value * -1 : value;
#else
        value = adc1_get_raw(ADC1_CHANNEL_0);
#endif
        if (xQueueSendToBack(queueRead, &value, 1000 / portTICK_RATE_MS) != pdTRUE)
        {
            printf("[ERROR] Writing\n");
        }
#ifdef DEBUG
        printf("[DEBUG][adc1Task()] The adc1 value is: %d\n", value);
#endif
    }
    vTaskDelete(NULL);
}

void convertData(void *pvParameters)
{
    int dato;
    char String[VOLTAGE_MSG_SZ];
    float T = 0.00;
    while (1)
    {
        if (xQueueReceive(queueRead, &dato, 10000 / portTICK_RATE_MS) == pdTRUE)
        {
            T = (3.3 * dato) / 4095;
            sprintf(String, "%1.2f\r", T);
#ifdef DEBUG
            printf("[DEBUG]%s\n", String);
#endif
        }
        else
        {
            printf("[Error] Reading Data\n");
        }
        if (xQueueSendToBack(queueVoltage, &String, 1000 / portTICK_RATE_MS) != pdTRUE)
        {
            printf("[ERROR] Writing voltage\n");
        }
        printf("[convertData()] adc1 value is: %d\n", dato);
        vTaskDelay(500 / portTICK_RATE_MS);
    }
    vTaskDelete(NULL);
}

static void uart_task(void *pvParameters)
{
    const uart_port_t uart_num = UART_NUM_1;
    uart_config_t uart_config = {
        .baud_rate = 9600,
        .data_bits = UART_DATA_8_BITS,
        .parity = UART_PARITY_DISABLE,
        .stop_bits = UART_STOP_BITS_1,
        .flow_ctrl = UART_HW_FLOWCTRL_DISABLE,
        .rx_flow_ctrl_thresh = 122,
        .use_ref_tick = false};
    uart_param_config(uart_num, &uart_config);
    uart_set_pin(uart_num, TXD, RXD, RTS, CTS);
    uart_driver_install(uart_num, BUFF_SZ * 2, 0, 0, NULL, 0);
    const char *uartAdcMessage = "ADC1 Lecture\tV = ";
    char Rx[VOLTAGE_MSG_SZ];
    while (1)
    {
        if (xQueueReceive(queueVoltage, &Rx, 10000 / portTICK_RATE_MS) == pdTRUE)
        {
            uart_write_bytes(uart_num, (const char *)uartAdcMessage, strlen(uartAdcMessage));
            uart_write_bytes(uart_num, (char *)Rx, strlen(Rx));
        }
    }
    vTaskDelete(NULL);
}

int start_app()
{
    queueRead = xQueueCreate(QUEUE_READ_MSG_SZ, READ_MSG_SZ);
    queueVoltage = xQueueCreate(QUEUE_VOLTAGE_MSG_SZ, VOLTAGE_MSG_SZ);

    xTaskCreate(adc1Task, "Adc 1 Task", 1024 * 3, NULL, 5, NULL);
    xTaskCreate(convertData, "Convert Data", 1024 * 3, NULL, 10, NULL);
    xTaskCreate(uart_task, "UART task", 1024, NULL, 5, NULL);
    return 0;
}

#endif // !_05_FREERTOS_ADC_UART_H_