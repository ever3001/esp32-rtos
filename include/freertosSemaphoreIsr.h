#ifndef _03_FREERTOS_SEMAPHORE_ISR_H_
#define _03_FREERTOS_SEMAPHORE_ISR_H_

#include <stdio.h>

/* Freertos Headers */
#include "esp_system.h"
#include "nvs_flash.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/semphr.h"
#include "driver/gpio.h"

#define ESP_INTR_FLAG_DEFAULT (0)
#define BTN_PIN GPIO_NUM_13


SemaphoreHandle_t xSemaphore = NULL;

void IRAM_ATTR pulsador_isr_handler(void * args)
{
    xSemaphoreGiveFromISR(xSemaphore, NULL);
}

void init_GPIO(){
    gpio_pad_select_gpio(BTN_PIN);
    gpio_set_direction(BTN_PIN, GPIO_MODE_INPUT);
    gpio_install_isr_service(ESP_INTR_FLAG_DEFAULT);
    gpio_isr_handler_add(BTN_PIN, pulsador_isr_handler, NULL);
    gpio_set_intr_type(BTN_PIN, GPIO_INTR_NEGEDGE);
}

void task_pulsador(void* args)
{
    while (1)
    {
        if(xSemaphoreTake(xSemaphore, portMAX_DELAY) == pdTRUE)
        {
            printf("Pulsador presionado! \n");
        }
    }
    vTaskDelete(NULL);
    
}



int start_app(){
    nvs_flash_init();
    init_GPIO();
    xSemaphore = xSemaphoreCreateBinary();
    xTaskCreate(task_pulsador, "Task pulsador", 2048, NULL, 5, NULL);


    return 0;
}



#endif // !_03_FREERTOS_SEMAPHORE_ISR_H_