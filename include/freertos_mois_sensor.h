#ifndef _FREERTOS_MOISTURE_SENSOR_H_
#define _FREERTOS_MOISTURE_SENSOR_H_
/** Standard libraries*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/** Freertos Headers */
#include "esp_system.h"
#include "freertos/FreeRTOS.h"
#include "freertos/queue.h"
#include "freertos/task.h"
#include "nvs_flash.h"

/** GPIO and drivers */
#include "driver/adc.h"

#define DEBUG

#define QUEUE_READ_MSG_SZ (10)
#define TIME_READ_DATA_IN_MS (pdMS_TO_TICKS(1000)) // 1000 ms = 1 sec
#define TIME_QUEUE_MAX_IN_MS (pdMS_TO_TICKS(5000))
#define NO_OF_SAMPLES (64) // Multisampling

xQueueHandle queueMoistureSensorVal;

void get_moisture_sensor_adc_val_task(void *args) {
  adc1_config_width(ADC_WIDTH_BIT_12);
  adc1_config_channel_atten(ADC1_CHANNEL_6, ADC_ATTEN_DB_11);
  int adc_reading;
  // Calculate the task delay
  const TickType_t xDelay = TIME_READ_DATA_IN_MS;
  while (1) {
    adc_reading = 0;
    // Multisampling
    for (size_t i = 0; i < NO_OF_SAMPLES; ++i) {
      adc_reading += adc1_get_raw(ADC1_CHANNEL_6);
    }
    adc_reading /= NO_OF_SAMPLES;
    if (xQueueSendToBack(queueMoistureSensorVal, &adc_reading,
                         TIME_QUEUE_MAX_IN_MS) != pdTRUE) {
      printf("[ERROR] Writing\n");
    }
#ifdef DEBUG
    printf("[DEBUG][get_moisture_sensor_adc_val()] The adc1 value is: %d\n",
           adc_reading);
#endif
    // Block task for time calculated
    vTaskDelay(xDelay);
  }
  vTaskDelete(NULL);
}

void process_moisture_sensor_val(void *args) {
  int adc_reading = 0;
  while (1) {
    if (xQueueReceive(queueMoistureSensorVal, &adc_reading,
                      TIME_QUEUE_MAX_IN_MS) == pdTRUE) {
      // Add here processing
      printf(
          "[process_moisture_sensor_val()] The value of mois sensor is: %d\n",
          adc_reading);
    } else {
      printf("[ERROR] read_task");
    }
  }
  vTaskDelete(NULL);
}

int start_app() {
  queueMoistureSensorVal = xQueueCreate(QUEUE_READ_MSG_SZ, sizeof(int));

  xTaskCreate(get_moisture_sensor_adc_val_task, "Get Moisture Value Task",
              1024 * 3, NULL, tskIDLE_PRIORITY, NULL);
  xTaskCreate(process_moisture_sensor_val, "Process Moisture Value Task",
              1024 * 3, NULL, tskIDLE_PRIORITY, NULL);
  return 0;
}

#endif // !_FREERTOS_MOISTURE_SENSOR_H_