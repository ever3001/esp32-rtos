#ifndef _04_FREERTOS_MUTEX_H_
#define _04_FREERTOS_MUTEX_H_
/** Standard libraries*/
#include <stdio.h>

/** Freertos Headers */
#include "esp_system.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/semphr.h"

xSemaphoreHandle xMutex;

void task_1(void *pvParameter)
{
    while (1)
    {
        if(xMutex != NULL)
        {
            if(xSemaphoreTake(xMutex, portMAX_DELAY) == pdTRUE)
            {
                printf("************************************\n");
                printf("Executing task 1\n");
                printf("************************************\n");
            }
            xSemaphoreGive(xMutex);
        }
        vTaskDelay(1000 / portTICK_PERIOD_MS);
    }
    vTaskDelete(NULL);
}


void task_2(void *pvParameter)
{
    while (1)
    {
        if(xMutex != NULL)
        {
            if(xSemaphoreTake(xMutex, portMAX_DELAY) == pdTRUE)
            {
                printf("####################################\n");
                printf("Executing task 2\n");
                printf("####################################\n");
            }
            xSemaphoreGive(xMutex);
        }
        vTaskDelay(1000 / portTICK_PERIOD_MS);
    }
    vTaskDelete(NULL);
}

void task_3(void *pvParameter)
{
    while (1)
    {
        if(xMutex != NULL)
        {
            if(xSemaphoreTake(xMutex, portMAX_DELAY) == pdTRUE)
            {
                printf("------------------------------------\n");
                printf("Executing task 3\n");
                printf("------------------------------------\n");
            }
            xSemaphoreGive(xMutex);
        }
        vTaskDelay(1000 / portTICK_PERIOD_MS);
    }
    vTaskDelete(NULL);
}

void task_4(void *pvParameter)
{
    while (1)
    {
        if(xMutex != NULL)
        {
            if(xSemaphoreTake(xMutex, portMAX_DELAY) == pdTRUE)
            {
                printf("====================================\n");
                printf("Executing task 4\n");
                printf("====================================\n");
            }
            xSemaphoreGive(xMutex);
        }
        vTaskDelay(1000 / portTICK_PERIOD_MS);
    }
    vTaskDelete(NULL);
}

int start_app()
{
    xMutex = xSemaphoreCreateMutex();

    xTaskCreate(&task_1, "task_1", 1024, NULL, 5, NULL);
    xTaskCreate(&task_2, "task_2", 1024, NULL, 5, NULL);
    xTaskCreate(&task_3, "task_3", 1024, NULL, 5, NULL);
    xTaskCreate(&task_4, "task_4", 1024, NULL, 5, NULL);
    return 0;
}

#endif // !_04_FREERTOS_MUTEX_H_