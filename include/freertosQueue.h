#ifndef _02_FREERTOS_QUEUE_H_
#define _02_FREERTOS_QUEUE_H_

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "esp_system.h"
#include "nvs_flash.h"

#include <iostream>

/* Freertos Headers */
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"
//#include "freertos/heap_regions.h"


#define QUEUE_SZ    (20) // 20 messages
#define MSG_SZ      (32)  // 16 characters for each message

xQueueHandle msg_queue;

void write_task_01(void *pvParameters)

{
    char message[MSG_SZ];
    while (1)
    {
        strcpy(message,"Write Message from Task_01\n");
        if(xQueueSendToBack(msg_queue,& message, 2000/portTICK_RATE_MS) != pdTRUE)
        {
            printf("[ERROR] write_task_01\n");
        }
        vTaskDelay(2000 / portTICK_PERIOD_MS);
    }
    vTaskDelete(NULL);
}

void write_task_02(void *pvParameters)

{
    char message[MSG_SZ];
    while (1)
    {
        strcpy(message,"Write Message from Task_02\n");
        if(xQueueSendToBack(msg_queue,& message, 2000/portTICK_RATE_MS) != pdTRUE)
        {
            printf("[ERROR] write_task_02\n");
        }
        vTaskDelay(2000 / portTICK_PERIOD_MS);
    }
    vTaskDelete(NULL);
}

void write_task_03(void *pvParameters)

{
    char message[MSG_SZ];
    while (1)
    {
        strcpy(message,"Write Message from Task_03\n");
        if(xQueueSendToBack(msg_queue,& message, 2000/portTICK_RATE_MS) != pdTRUE)
        {
            printf("[ERROR] write_task_03\n");
        }
        vTaskDelay(2000 / portTICK_PERIOD_MS);
    }
    vTaskDelete(NULL);
}

void read_task(void *pvParameters)
{
    size_t i;
    char message_in[MSG_SZ];
    while (1)
    {
        if(xQueueReceive(msg_queue, message_in, 10000/portTICK_RATE_MS) == pdTRUE)
        {
            for (i = 0; i < strlen(message_in); ++i)
            {
                printf("%c", message_in[i]);
            }
        }else
        {
            printf("[ERROR] read_task");
        }
        
    }
    vTaskDelete(NULL);
}



int start_app(){
    nvs_flash_init();

    msg_queue = xQueueCreate(QUEUE_SZ, MSG_SZ);

    xTaskCreate(&read_task, "Read task", 1024, NULL, 5, NULL);
    xTaskCreate(&write_task_01, "Write task 1", 1024, NULL, 1, NULL);
    xTaskCreate(&write_task_02, "Write task 2", 1024, NULL, 1, NULL);
    xTaskCreate(&write_task_03, "Write task 3", 1024, NULL, 1, NULL);

    return 0;
}



#endif // !_02_FREERTOS_QUEUE_H_