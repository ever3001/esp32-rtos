#ifndef _01_FREERTOS_TASK_H_
#define _01_FREERTOS_TASK_H_

#include <stdio.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_system.h"
#include "nvs_flash.h"


void task_1(void *pvParameter)
{
    while(1) {
      printf("Execute tast 1\n");
        vTaskDelay(1000 / portTICK_PERIOD_MS);

    }
    vTaskDelete(NULL);
}

void task_2(void *pvParameter)
{
    while(1) {
      printf("Execute tast 2\n");
        vTaskDelay(1000 / portTICK_PERIOD_MS);

    }
    vTaskDelete(NULL);
}

void task_3(void *pvParameter)
{
    while(1) {
      printf("Execute tast 3\n");
        vTaskDelay(1000 / portTICK_PERIOD_MS);

    }
    vTaskDelete(NULL);
}

void task_4(void *pvParameter)
{
    while(1) {
      printf("Execute tast 4\n");
        vTaskDelay(1000 / portTICK_PERIOD_MS);

    }
    vTaskDelete(NULL);
}

int start_app(){
    nvs_flash_init();
    xTaskCreate(&task_1, "task_1", 1024, NULL, 1, NULL);
    xTaskCreate(&task_2, "task_2", 1024, NULL, 2, NULL);
    xTaskCreate(&task_3, "task_3", 1024, NULL, 3, NULL);
    xTaskCreate(&task_4, "task_4", 1024, NULL, 4, NULL);
    return 0;
}

#endif // !_01_FREERTOS_TASK_H_